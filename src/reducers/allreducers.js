import { combineReducers } from 'redux';
import login_reducer from "./login_reducer";
import register_reducer from './registerReducer';

const allreducers = combineReducers({
    editlogin:login_reducer,    
    editregister:register_reducer
});

export default allreducers;