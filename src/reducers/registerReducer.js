import ACTION_REGISTER_NAME from '../actions/action_register';
import initialState from '../actions/initialState';


export default function register_reducer(state = initialState.user_register, action) {

    switch (action.type) {
        case ACTION_REGISTER_NAME:
            //return action.user_register;    /// не помогло
            return { ...state, user_register: action.user_register };         
        default:
            return state;
    }
}
