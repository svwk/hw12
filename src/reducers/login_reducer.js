import ACTION_LOGIN_NAME from '../actions/action_login';
import initialState from '../actions/initialState';



export default function login_reducer(state = initialState.user_login, action) {

    switch (action.type) {
        case ACTION_LOGIN_NAME:
            //state.user_login = action.user_login;
            //return state;
            return { ...state, user_login: action.user_login  };
        default:
            return state;
    }
}
