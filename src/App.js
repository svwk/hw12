import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { Link as MatLink } from '@material-ui/core';
import { Link as RoutLink } from 'react-router-dom';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import HomeIcon from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';

import './styles/App.css';
import linkStyle from './styles/linkStyle';
import useStyles from './styles/NaviStyle';
import NotFound from './components/NotFoundPage';
import { HomePageContainer, RegisterPageContainer, LoginPageContainer } from './components/containers';


function App() {
	const classes = useStyles();
	return (
		<>
			<BrowserRouter>
				<AppBar position="static" color="primary">
					<Toolbar>
						<IconButton edge="start" color="inherit" aria-label="menu" component={RoutLink} to="/">
							<HomeIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title} style={linkStyle} component={RoutLink} to="/">
							На главную
						</Typography>
						<MatLink variant="h6" style={linkStyle} component={RoutLink} to="/some-page">Несуществующая страница</MatLink>
						<MatLink variant="h6" style={linkStyle} component={RoutLink} to="/login">Войти</MatLink>
						<MatLink variant="h6" style={linkStyle} component={RoutLink} to="/register">Регистрация</MatLink>						
					</Toolbar>
				</AppBar>

				<Switch>
					<Route path="/" exact><HomePageContainer />	</Route>
					<Route path="/login">  <LoginPageContainer /> </Route>
					<Route path="/register"><RegisterPageContainer /> </Route>
					<Route path="*" component={NotFound} />
				</Switch>
			</BrowserRouter>
			<Box mt={5}>
				<Typography variant="body2" color="textSecondary" align="center">
					{'Copyright © '}
					<MatLink color="inherit" href="https://material-ui.com/">
						Наш замечательный сайт
					</MatLink>{' '}
					{new Date().getFullYear()}
					{'.'}
				</Typography>
			</Box>
		</>
	);
}

export default App;
