const ACTION_REGISTER_NAME = "ACTION_REGISTER";

function  action_register(value){
	return {
        type: ACTION_REGISTER_NAME,
        user_register: value        
	};
}

export default action_register;