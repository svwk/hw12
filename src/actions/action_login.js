const ACTION_LOGIN_NAME = "ACTION_LOGIN";

function  action_login(value){
	return {
        type: ACTION_LOGIN_NAME,
        user_login: value          
	};
}

export default action_login;