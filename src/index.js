import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';


import App from './App';
import { createStore } from 'redux';
import userReducer from './reducers/userReducer';
// import allreducers from './reducers/allreducers';
// import loginReducer from "./reducers/login_reducer";
// import registerReducer from './reducers/registerReducer';
// import { combineReducers } from 'redux';

const store = createStore(userReducer);
//const store = createStore(allreducers);  ///  не работает???!!!!

ReactDOM.render(
  <React.StrictMode>
    <Provider store = {store}>    
    <App />    
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
