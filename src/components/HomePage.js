import React from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import siteStyle from '../styles/siteStyles';



class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            logined: this.props.user_login.logined,
            login_Name: this.props.user_login.login_Name
        };
    }
    
    render() {
        const classes=siteStyle.homeStyles();
        
        const logined=this.state.logined;
        const login_Name=this.state.login_Name;
		return (
        <>             
            {!logined  ?
                <>
                    <Typography variant="h3" className={classes.title}>
                        Добрый день, Гость!
                    </Typography>
                    <br />                   
                    <Divider />
                    <br />   
                </>
                :
                <>
                    <Typography variant="h3" className={classes.title}>
                        Добрый день, {login_Name}!
                    </Typography>
                    <br />
                    <Divider />
                    <br />                   
                </>
            }
        </>
    );
        }
}

export default HomePage;
