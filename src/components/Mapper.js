import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import HomePage from './HomePage';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import action_login from '../actions/action_login';
import action_register from '../actions/action_register';




export default function map(component) {    
    switch (component) {
        case "LoginPage": {
            const mapStateToProps= (state)=> {return{user_login: state.user_login}};
            const mapDispatchToProps=(dispatch)=>{return{
				login_reducer: bindActionCreators(action_login, dispatch)
			}};
            return connect(mapStateToProps, mapDispatchToProps)(LoginPage);            
        }
        case "HomePage": {
            const mapStateToProps= (state)=> {return {user_login: state.user_login} };             		
            return connect(mapStateToProps,null)(HomePage);            
        }
        case "RegisterPage": {
            const mapStateToProps= (state)=> {return{user_register: state.user_register}};
            const mapDispatchToProps=(dispatch)=>{return{
				register_reducer: bindActionCreators(action_register, dispatch)
			}};
            return connect(mapStateToProps, mapDispatchToProps)(RegisterPage);            
        }
        default: return undefined;
    }
   
}
