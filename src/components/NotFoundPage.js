import React from 'react';
import useStyles from '../styles/ErrorStyle';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

const NotFoundPage = () => {
    const classes = useStyles();
    return(
        <>
            <br/>
            <Typography variant="h3" className={classes.title}>
                Ошибка 404
			</Typography>
            <br/>
            <Divider />
            <br/>
            <Typography variant="h4" color="textSecondary" className={classes.title}>
                К сожалению, запрашиваемая страница не найдена
            </Typography>
        </>     
    )
}    

export default NotFoundPage;
