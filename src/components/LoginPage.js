import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Redirect } from 'react-router-dom'

import siteStyle from '../styles/siteStyles';


class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            logined: this.props.user_login.logined,
            login_Name: this.props.user_login.login_Name,
            login_password: this.props.user_login.login_password ,
            isdone:false           
        };
    }

    handleLoginAction = () => {
        
        let user_login = {            
            login_Name: this.state.login_Name,
            login_password: this.state.login_password,
            logined: true
        }        
        this.props.login_reducer(user_login);
        alert('пользователь ' + this.state.login_Name + ' вошел');
           
    }

    handleUnLoginAction = () => {
        let data = {
            login_Name: '',
            login_password: '',
            logined: false
        }
        this.props.login_reducer(data);
        console.log('пользователь ' + this.state.login_Name + 'вышел');
        fetch('/');
    }

    handleLoginNameChanged = (event) => {
        this.setState({ login_Name: event.target.value });
    }
    handlePasswordChanged = (event) => {
        this.setState({ login_password: event.target.value });
    }


    

    render() { 
        const classes=siteStyle.loginStyles();
        const logined=this.state.logined;
        const login_Name=this.state.login_Name;
        const login_password=this.state.login_password;
        
        return (
            <Container component="main" maxWidth="xs">
                {logined ?
                  <Redirect to='/' />  
                  :
                  <></>
                }
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        ВХОД
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="login_Name"
                            label="Логин"
                            name="login_Name"
                            autoFocus
                            value={login_Name}
                            onChange={this.handleLoginNameChanged}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="login_password"
                            label="Пароль"
                            type="password"
                            id="login_password"
                            autoComplete="current-password"
                            value={login_password}
                            onChange={this.handlePasswordChanged}
                        />
                        <br/>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.handleLoginAction}
                        >Войти</Button>
                        <br/>
                        <br/>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.handleUnLoginAction}
                        >Выйти</Button>
                    </form>
                </div>
            </Container>
        );
    }
}

export default LoginPage;


