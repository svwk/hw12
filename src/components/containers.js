import map from './Mapper';

export const HomePageContainer=map('HomePage');
export const RegisterPageContainer=map('RegisterPage');
export const LoginPageContainer=map('LoginPage');
