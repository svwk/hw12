import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import siteStyle from '../styles/siteStyles';

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      registered: this.props.user_register.registered,
      register_name: this.props.user_register.register_name,
      register_email: this.props.user_register.register_email,
      register_password: this.props.user_register.register_password,
      isdone: false
    }
  };

  handleRegAction = () => {
    let user_register = {
      registered: true,
      register_name: this.state.register_name,
      register_email: this.state.register_email,
      register_password: this.state.register_password
    }
    this.props.register_reducer(user_register);
    alert('пользователь ' + this.state.register_name + ' зарегистрировался');
  }

  handleNameChanged = (event) => {
    this.setState({ register_name: event.target.value });
  }
  handlePasswordChanged = (event) => {
    this.setState({ register_password: event.target.value });
  }

  handleEmailNameChanged = (event) => {
    this.setState({ register_email: event.target.value });
  }

  render() {
    const classes = siteStyle.registerStyles();
    const registered = this.state.registered;
    const register_name = this.state.register_name;
    const register_email = this.state.register_email;
    const register_password = this.state.register_password;

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Регистрация
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="register_name"
                  variant="outlined"
                  required
                  fullWidth
                  id="register_name"
                  label="Имя"
                  autoFocus
                  value={register_name}
                  onChange={this.handleNameChanged}
                />
              </Grid>             
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="register_email"
                  label="Email адрес"
                  name="register_email"
                  autoComplete="email"
                  value={register_email}
                  onChange={this.handleEmailNameChanged}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="register_password"
                  label="Пароль"
                  type="password"
                  id="register_password"
                  autoComplete="current-password"
                  value={register_password}
                  onChange={this.handlePasswordChanged}
                />
              </Grid>              
            </Grid>
            <br/>
            <Button
              type="button"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.handleRegAction}
            >Зарегистрироваться</Button>
          </form>
        </div>
      </Container>
    )
  };
}

export default RegisterPage;

