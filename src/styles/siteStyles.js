import { makeStyles } from '@material-ui/core/styles';

export default class siteStyle {

    static homeStyles = () => {
        return (
            makeStyles((theme) => ({
                paper: {
                    marginTop: theme.spacing(8),
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                },
                avatar: {
                    margin: theme.spacing(1),
                    backgroundColor: theme.palette.secondary.main,
                },
                form: {
                    width: '100%', // Fix IE 11 issue.
                    marginTop: theme.spacing(1),
                },
                submit: {
                    margin: theme.spacing(3),
                },
                title: {
                    textAlign: 'center'
                }
            })))
    };

    static errorStyles = () => {
        return (
            makeStyles((theme) => ({
                root: {
                    flexGrow: 1,
                },
                menuButton: {
                    marginRight: theme.spacing(2),
                },
                title: {
                    flexGrow: 1,
                    textAlign: 'center',
                    paddingTop: theme.spacing(10)
                },
            })));
    }

    static linkStyle = {
        color: 'white',
        marginRight: '20px',
        textDecoration: 'none'
    };

    static loginStyles = () => {
        return (
            makeStyles((theme) => ({
                paper: {
                    marginTop: theme.spacing(8),
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                },
                avatar: {
                    margin: theme.spacing(1),
                    backgroundColor: theme.palette.secondary.main,
                },
                form: {
                    width: '100%', // Fix IE 11 issue.
                    marginTop: theme.spacing(1),
                },
                submit: {
                    margin: theme.spacing(3, 0, 2),
                },
            })))
    };

    static naviStyles = () => {
        return (
            makeStyles((theme) => ({
                root: {
                    flexGrow: 1,
                },
                menuButton: {
                    marginRight: theme.spacing(2),
                },
                title: {
                    flexGrow: 1,
                },
            })))
    };

    static registerStyles = () => {
        return (
            makeStyles((theme) => ({
                paper: {
                    marginTop: theme.spacing(8),
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                },
                avatar: {
                    margin: theme.spacing(1),
                    backgroundColor: theme.palette.secondary.main,
                },
                form: {
                    width: '100%', // Fix IE 11 issue.
                    marginTop: theme.spacing(3),
                },
                submit: {
                    margin: theme.spacing(3, 0, 2),
                },
            })))
    };


}


