import { makeStyles } from '@material-ui/core/styles';


const useStyles = () => {
    return (
        makeStyles((theme) => ({
            root: {
                flexGrow: 1,
            },
            menuButton: {
                marginRight: theme.spacing(2),
            },
            title: {
                flexGrow: 1,
                textAlign: 'center',
                paddingTop: theme.spacing(10)
            },
        })));
}

export default useStyles;